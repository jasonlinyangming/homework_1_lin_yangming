# homework_1
Homework 1 
CSCIE-55
Fall 2019
Fred Evers

Homework 1: Elevator Simulation 1

Due: Monday, September 16th 11:59 Eastern time
Grading: 15%
Last Modified: Friday, 6-Sept-2019 7:39:58 EDT

Overview: 
The requirements
1. take one round trip from bottom to top
2. print out guests remain in the elevator per floor
3. release guest only on destination floors, otherwise continue to move
4. when returned to ground floor release all guests and stops

Meeting the requirements
1. use boolean to track if the top if reached, if yes begin descend
2. calls the toString() after guest exited or before continue to move()
3. if the guestPerFlr[] indicates guest(s) is(are) exiting this floor, release, otherwise move()
4. when int crntFlr becomes 0 empty the array and exit program
5. no current requirement to handle guests entering on 1st floor
