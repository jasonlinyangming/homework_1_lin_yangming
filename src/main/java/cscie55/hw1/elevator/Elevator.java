package cscie55.hw1.elevator;
/***
 * Define a constant, (i.e., a static final field), for the number of floors in the building, and set it to 7. Use it where appropriate.
 * Define a field for tracking the Elevator's current floor
 * Define a field for tracking the Elevator's direction of travel.
 * Define an array-valued field for tracking, for each floor, the number of passengers destined for that floor.
 * Define a move() method which, when called, modifies the Elevator's state, (i.e., updates the fields appropriately):
 * Increments/decrements the current floor, i.e. the Elevator moves one floor at a time.
 * Modifies the direction of travel, if the ground floor or top floor has been reached.
 * Clears the array entry tracking the number of passengers destined for the floor that the elevator has just arrived at.
 * For each floor, if it stops or not, the move() method prints out the status of the Elevator [see toString() method below]
 * Define a boardPassenger(int destinationFloor) method
 * This method adds to the Elevator one passenger destined for the indicated floor.
 * Define a toString() method to aid in debugging and testing.
 * The String returned by toString() should indicate the number of passengers on board, and the current floor. So each time the toString() method is called, it returns something like: Floor n: n passengers
 */

public class Elevator {
    private static final int flrInBuilding = 7;
    private int crntFlr = 0;
    private boolean isGoingUp = true;
    private int[] guestPerFlr = new int[flrInBuilding];
    private int guestInElevator = 0;


    /*when called, modifies the Elevator's state
        The Elevator starts on the ground floor and goes up. When it reaches the top floor it starts going down.
        When it has gone to one cycle, from top to bottom,
        it stops and should empty any passengers.
        When moving in either direction, it continues moving, stopping only at destination floors.
        That is, a stop on a floor is necessary if there are passengers destined for that floor.
        When the Elevator stops on a destination floor, it should discharge the passengers
        who wanted to go there and change its state to reflect the fact that the floor is no longer a destination.
        Then it moves on to the next floor, ultimately stopping at the bottom.
     */
    public void move() {
        //Increments/decrements the current floor, i.e. the Elevator moves one floor at a time.
        if (isGoingUp) {
            crntFlr = crntFlr + 1;
        } else {
            crntFlr = crntFlr - 1;
        }
        //Modifies the direction of travel, if the ground floor or top floor has been reached.
        if (crntFlr == 6) isGoingUp = false;
        //When it has gone to one cycle, from top to bottom,it stops and should empty any passenger
        if (crntFlr == 0) {
            isGoingUp = true;
            for (int i = 0;i<flrInBuilding;i++) {
                guestPerFlr[crntFlr] = 0;
            }
            System.out.println(toString());
            System.exit(0);
        }
        //continues moving, stopping only at destination floors
        if(guestPerFlr[crntFlr] == 0){
            System.out.println(toString());
            move();
        }else {
            //Clears the array entry tracking the number of passengers destined for the floor that the elevator has just arrived at.
            guestInElevator = guestInElevator - guestPerFlr[crntFlr];
            guestPerFlr[crntFlr] = 0;
            //For each floor, if it stops or not, the move() method prints out the status of the Elevator [see toString() method below]
            System.out.println(toString());
        }
        //continue to move
        if (crntFlr != 0) move();
    }

    /*This method adds to the Elevator one passenger destined for the indicated floor.
     */
    public void boardPassenger(int destinationFloor) {
        guestPerFlr[destinationFloor - 1] = guestPerFlr[destinationFloor - 1] + 1;
        guestInElevator = guestInElevator + 1;
    }

    /*The String returned by toString() should indicate the number of passengers on board, and the current floor.
    So each time the toString() method is called, it returns something like: Floor n: n passengers
     */
    public String toString() {
        return "Floor " + (crntFlr + 1) + ": " + guestInElevator + " passengers";
    }

}