package cscie55.hw1.elevator;

public class ElevatorTest {
    public static void main(String[] args) {
        //Create an instance of an Elevator object.
        Elevator elevator = new Elevator();
        //Board two passengers for the 3nd floor, and one for the 5th floor.
        elevator.boardPassenger(3);
        elevator.boardPassenger(3);
        elevator.boardPassenger(5);
        //Move the Elevator from the ground floor to the top floor, and then back to the ground floor.
        //Print the state of the elevator before the first move, and after each move. Your output should look like this:
        //Floor 1: 3 passengers
        //Floor 2: 3 passengers
        //Floor 3: 1 passenger
        //Floor 4: 1 passenger
        //Floor 5: 0 passengers
        //Floor 6: 0 passengers
        //Floor 7: 0 passengers
        //Floor 6: 0 passengers
        //Floor 5: 0 passengers
        //Floor 4: 0 passengers
        //Floor 3: 0 passengers
        //Floor 2: 0 passengers
        //Floor 1: 0 passengers
        System.out.println(elevator.toString());
        elevator.move();
    }
}
